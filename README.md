# azrs-tracking

Projekat iz predmeta Alati za razvoj softvera. Alati su primenjeni na fork projekta iz predmeta Razvoj softvera.
[number-one-uno](https://gitlab.com/nikolamajstorovich/number-one-uno)


## Alati

1. [CI](https://gitlab.com/nikolamajstorovich/azrs-tracking/-/issues/1)
2. [CMAKE](https://gitlab.com/nikolamajstorovich/azrs-tracking/-/issues/2)
3. [GIT](https://gitlab.com/nikolamajstorovich/azrs-tracking/-/issues/3)
4. [GIT-HOOKS](https://gitlab.com/nikolamajstorovich/azrs-tracking/-/issues/4)
5. [CLANG-TIDY](https://gitlab.com/nikolamajstorovich/azrs-tracking/-/issues/5)
6. [CLANG-FORMAT](https://gitlab.com/nikolamajstorovich/azrs-tracking/-/issues/6)
7. [GCOV](https://gitlab.com/nikolamajstorovich/azrs-tracking/-/issues/7)
8. [CLAZY](https://gitlab.com/nikolamajstorovich/azrs-tracking/-/issues/8)
9. [VALGRIND](https://gitlab.com/nikolamajstorovich/azrs-tracking/-/issues/9)
10. [DOCKER](https://gitlab.com/nikolamajstorovich/azrs-tracking/-/issues/10)

